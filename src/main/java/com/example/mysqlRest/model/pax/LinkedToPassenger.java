package com.example.mysqlRest.model.pax;

import com.fasterxml.jackson.annotation.JsonBackReference;

public interface LinkedToPassenger {
	@JsonBackReference
	public Passenger getPassenger();

	public void setPassenger(Passenger passenger);
}
