package com.example.mysqlRest.model.pax;

import com.example.mysqlRest.model.AbstractModel;

import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
public abstract class ManyToOneLinkedToPassenger extends AbstractModel implements LinkedToPassenger {
	@ManyToOne
	@JoinColumn(name = "passenger_id", nullable = false)
	protected Passenger passenger;

	public Passenger getPassenger() {
		return passenger;
	}

	public void setPassenger(Passenger passenger) {
		this.passenger = passenger;
	}
}
