package com.example.mysqlRest.model.pax;

import com.example.mysqlRest.model.enums.Title;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;

@Entity
@Data
@EqualsAndHashCode(callSuper=true)
@Table(name = "pax_name_data")
public class PaxName extends OneToOneLinkedToPassenger {
	private String firstName;
	private String lastName;
	@Enumerated(EnumType.STRING)
	private Title title;
}
