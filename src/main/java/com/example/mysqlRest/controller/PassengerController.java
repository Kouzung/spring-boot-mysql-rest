package com.example.mysqlRest.controller;

import com.example.mysqlRest.exception.ResourceNotFoundException;
import com.example.mysqlRest.model.pax.Passenger;
import com.example.mysqlRest.repository.PassengerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api")
public class PassengerController {

    @Autowired
    PassengerRepository PassengerRepository;

    @GetMapping("/Passengers")
    public List<Passenger> getAllPassengers() {
        return PassengerRepository.findAll();
    }

    @PostMapping("/Passengers")
    public Passenger createPassenger(@Valid @RequestBody Passenger Passenger) {
        return PassengerRepository.save(Passenger);
    }

    @GetMapping("/Passengers/{id}")
    public Passenger getPassengerById(@PathVariable(value = "id") Long PassengerId) {
        return PassengerRepository.findById(PassengerId)
                .orElseThrow(() -> new ResourceNotFoundException("Passenger", "id", PassengerId));
    }

    /*@PutMapping("/Passengers/{id}")
    public Passenger updatePassenger(@PathVariable(value = "id") Long PassengerId,
                                           @Valid @RequestBody Passenger PassengerDetails) {

        Passenger Passenger = PassengerRepository.findById(PassengerId)
                .orElseThrow(() -> new ResourceNotFoundException("Passenger", "id", PassengerId));

        Passenger.setTitle(PassengerDetails.getTitle());
        Passenger.setContent(PassengerDetails.getContent());

        Passenger updatedPassenger = PassengerRepository.save(Passenger);
        return updatedPassenger;
    }*/

    @DeleteMapping("/Passengers/{id}")
    public ResponseEntity<?> deletePassenger(@PathVariable(value = "id") Long PassengerId) {
        Passenger Passenger = PassengerRepository.findById(PassengerId)
                .orElseThrow(() -> new ResourceNotFoundException("Passenger", "id", PassengerId));

        PassengerRepository.delete(Passenger);

        return ResponseEntity.ok().build();
    }
}
